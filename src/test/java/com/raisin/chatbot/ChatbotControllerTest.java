package com.raisin.chatbot;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.raisin.chatbot.models.request.DialogFlow;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ChatbotController.class)
public class ChatbotControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private
    ObjectMapper objectMapper;
    private JacksonTester<DialogFlow> jsonTester;
    private DialogFlow dialogFlow;

    private String JSON_EXAMPLE = "{\n" +
            "  \"id\": \"0bc921e2-3c2e-4526-880d-bb039bb19560\",\n" +
            "  \"timestamp\": \"2018-01-26T14:24:48.999Z\",\n" +
            "  \"lang\": \"en\",\n" +
            "  \"result\": {\n" +
            "    \"source\": \"agent\",\n" +
            "    \"resolvedQuery\": \"I want to invest 10000 euro for a period of at least 10 months\",\n" +
            "    \"action\": \"register-customer\",\n" +
            "    \"actionIncomplete\": true,\n" +
            "    \"parameters\": {\n" +
            "      \"max-term\": 24,\n" +
            "      \"min-term\": 10,\n" +
            "      \"min-amount\": 10000\n" +
            "    },\n" +
            "    \"contexts\": [\n" +
            "      {\n" +
            "        \"name\": \"looking-for-deposit-offers_dialog_params_max-term\",\n" +
            "        \"parameters\": {\n" +
            "          \"min-amount.original\": \"10000\",\n" +
            "          \"min-term.original\": \"10\",\n" +
            "          \"min-term\": 10,\n" +
            "          \"max-term.original\": \"\",\n" +
            "          \"max-term\": \"\",\n" +
            "          \"min-amount\": 10000\n" +
            "        },\n" +
            "        \"lifespan\": 1\n" +
            "      },\n" +
            "      {\n" +
            "        \"name\": \"b752d58a-40ac-4622-aa34-713871278181_id_dialog_context\",\n" +
            "        \"parameters\": {\n" +
            "          \"min-amount.original\": \"10000\",\n" +
            "          \"min-term.original\": \"10\",\n" +
            "          \"min-term\": 10,\n" +
            "          \"max-term.original\": \"\",\n" +
            "          \"max-term\": \"\",\n" +
            "          \"min-amount\": 10000\n" +
            "        },\n" +
            "        \"lifespan\": 2\n" +
            "      },\n" +
            "      {\n" +
            "        \"name\": \"looking-for-deposit-offers_dialog_context\",\n" +
            "        \"parameters\": {\n" +
            "          \"min-amount.original\": \"10000\",\n" +
            "          \"min-term.original\": \"10\",\n" +
            "          \"min-term\": 10,\n" +
            "          \"max-term.original\": \"\",\n" +
            "          \"max-term\": \"\",\n" +
            "          \"min-amount\": 10000\n" +
            "        },\n" +
            "        \"lifespan\": 2\n" +
            "      }\n" +
            "    ],\n" +
            "    \"metadata\": {\n" +
            "      \"intentId\": \"b752d58a-40ac-4622-aa34-713871278181\",\n" +
            "      \"webhookUsed\": \"true\",\n" +
            "      \"webhookForSlotFillingUsed\": \"false\",\n" +
            "      \"intentName\": \"looking-for-deposit-offers\"\n" +
            "    },\n" +
            "    \"fulfillment\": {\n" +
            "      \"speech\": \"After how many months would you like to get your money back the latest?\",\n" +
            "      \"messages\": [\n" +
            "        {\n" +
            "          \"type\": 0,\n" +
            "          \"speech\": \"After how many months would you like to get your money back the latest?\"\n" +
            "        }\n" +
            "      ]\n" +
            "    },\n" +
            "    \"score\": 1\n" +
            "  },\n" +
            "  \"status\": {\n" +
            "    \"code\": 200,\n" +
            "    \"errorType\": \"success\",\n" +
            "    \"webhookTimedOut\": false\n" +
            "  },\n" +
            "  \"sessionId\": \"76f6b101-68f5-4073-99ae-af78e4bc6d6f\"\n" +
            "}";

    @Before
    public void setup() throws IOException {
        JacksonTester.initFields(this, objectMapper);
        dialogFlow = objectMapper.readValue(JSON_EXAMPLE, DialogFlow.class);
    }

    @Test
    public void testChatbotPost() throws Exception {
        mockMvc
                .perform(post("/offer").content(JSON_EXAMPLE).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
    }
}
