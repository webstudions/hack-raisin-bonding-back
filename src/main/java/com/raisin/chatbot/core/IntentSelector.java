package com.raisin.chatbot.core;

import com.raisin.chatbot.gateway.OrderGateway;
import com.raisin.chatbot.models.api.Offer;

import java.util.ArrayList;

public class IntentSelector {

    public static ArrayList<Offer> parseIntent(String intentName, String request){
        switch ( intentName ){
            case "Default Fallback Intent":
                return OrderGateway.getAllOffers( request );
            case "looking-for-deposit-offers":
                return OrderGateway.getAllOffers(request);
            case "Show Deposit":
                return OrderGateway.getDesposit( request.toString() );
            default:
                break;
        }
        return null;
    }
}
