package com.raisin.chatbot;

import com.raisin.chatbot.core.IntentSelector;
import com.raisin.chatbot.filter.OfferFilter;
import com.raisin.chatbot.gateway.OrderGateway;
import com.raisin.chatbot.models.api.Bank;
import com.raisin.chatbot.models.request.DialogFlow;
import com.raisin.chatbot.models.api.Offer;
import com.raisin.chatbot.models.response.BestOffer;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class ChatbotController {

    public static ArrayList<Offer> processInboundRequest(DialogFlow dialogFlow) {
        String intent = dialogFlow.getResult().getMetadata().getIntentName();
        return IntentSelector.parseIntent(intent, "de_DE");
    }

    @RequestMapping("/")
    public ArrayList<Offer> home(){
        return IntentSelector.parseIntent("Default Fallback Intent", "deu");
    }

    @RequestMapping(value = "/offer", method = RequestMethod.POST)
    public BestOffer selectBestOffer(@RequestBody  DialogFlow dialogFlow) {
        try{
            ArrayList<Offer> bestOfferArrayList = IntentSelector.parseIntent(
                dialogFlow.getResult().getMetadata().getIntentName(),
                "deu"
            );
            OfferFilter offerFilter = new OfferFilter( bestOfferArrayList );
            Offer selectedOffer = offerFilter.getBestOfferFor(
                    dialogFlow.getResult().getParameters().getMinTerm(),
                    dialogFlow.getResult().getParameters().getMaxTerm(),
                    dialogFlow.getResult().getParameters().getMinAmount()
            );

            Bank bank = OrderGateway.getBank(selectedOffer.getBankId());

            BestOffer bestOffer = new BestOffer();
            bestOffer.setCode(selectedOffer.getId());
            bestOffer.setBank(bank.getDisplayName());
            bestOffer.setRate(selectedOffer.getNominalInterestRate());
            bestOffer.setTerm(selectedOffer.getTermMonthsCount());
            bestOffer.setAmount(dialogFlow.getResult().getParameters().getMinAmount());
            return bestOffer;
        } catch(Exception ex){
            return new BestOffer();
        }
    }

    @RequestMapping(value = "/direct", method = RequestMethod.POST)
    public ArrayList<ResponseEntity> directRequest(@RequestBody RequestEntity request) {
        try{
            return OrderGateway.forwardRequest(request);
        }
        catch (Exception ex){
            return new ArrayList<>();
        }
    }
}
