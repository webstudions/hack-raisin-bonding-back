package com.raisin.chatbot.gateway;

import com.raisin.chatbot.models.api.Bank;
import com.raisin.chatbot.models.api.Offer;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;

public class OrderGateway {

    public static ArrayList<Offer> getAllOffers(String countryCode){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("Authorization","Bearer bonding2");
        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<ArrayList<Offer>> offersEntity = restTemplate .exchange("https://bonding.testraisin.com/rcba/v1/offer/" + countryCode + "?locale=de_DE",
                HttpMethod.GET, entity, new ParameterizedTypeReference<ArrayList<Offer>>() {
                });
        return offersEntity.getBody();
    }

    public static Bank getBank(String bankID){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("Authorization","Bearer bonding2");
        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<Bank> offersEntity = restTemplate .exchange("https://bonding.testraisin.com/rcba/v1/bank/" + bankID + "?locale=de_DE",
                HttpMethod.GET, entity, new ParameterizedTypeReference<Bank>() {
                });
        return offersEntity.getBody();
    }

    public static <T> ArrayList<T> getDesposit ( String despoistID ){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("Authorization","Bearer bonding2");
        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<ArrayList<T>> offersEntity = restTemplate .exchange("https://bonding.testraisin.com/rcba/v1/deposit/" + despoistID + "?locale=de_DE",
                HttpMethod.GET, entity, new ParameterizedTypeReference<ArrayList<T>>() {
                });
        return offersEntity.getBody();
    }

    public static ArrayList<ResponseEntity> forwardRequest(RequestEntity request) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = request.getHeaders();
        headers.add("Authorization","Bearer bonding2");

        ResponseEntity<ArrayList<ResponseEntity>> offersEntity =
            restTemplate.exchange(
                    request.getUrl(),
                    request.getMethod(),
                    request,
                    new ParameterizedTypeReference<ArrayList<ResponseEntity>>() {
                    });
        return offersEntity.getBody();
    }
}
