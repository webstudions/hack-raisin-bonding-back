package com.raisin.chatbot.models.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Despoist {

    @JsonProperty
    private String id;

    @JsonProperty
    private String order_id;

    @JsonProperty
    private String customer_id;

    @JsonProperty
    private String offer_id;

    @JsonProperty
    private String offer_type;

    @JsonProperty
    private BankID bank_id;

    @JsonProperty
    private float nominal_interest_rate;

    @JsonProperty
    private int term_months_count;

    @JsonProperty
    private int amount;

    @JsonProperty
    private String placement_date;

    @JsonProperty
    private String maturity_date;

    @JsonProperty
    private float total_payout_amount;

    @JsonProperty
    private float interest_amount;

    @JsonProperty
    private float source_tax;

    @JsonProperty
    private float payout_exchange_rate;

    @JsonProperty
    private String payout_exchange_date;

    @JsonProperty
    private float payout_date;

    @JsonProperty
    private float deposit_status;

    @JsonProperty
    private Documents documents;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getOffer_id() {
        return offer_id;
    }

    public void setOffer_id(String offer_id) {
        this.offer_id = offer_id;
    }

    public String getOffer_type() {
        return offer_type;
    }

    public void setOffer_type(String offer_type) {
        this.offer_type = offer_type;
    }

    public BankID getBank_id() {
        return bank_id;
    }

    public void setBank_id(BankID bank_id) {
        this.bank_id = bank_id;
    }

    public float getNominal_interest_rate() {
        return nominal_interest_rate;
    }

    public void setNominal_interest_rate(float nominal_interest_rate) {
        this.nominal_interest_rate = nominal_interest_rate;
    }

    public int getTerm_months_count() {
        return term_months_count;
    }

    public void setTerm_months_count(int term_months_count) {
        this.term_months_count = term_months_count;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getPlacement_date() {
        return placement_date;
    }

    public void setPlacement_date(String placement_date) {
        this.placement_date = placement_date;
    }

    public String getMaturity_date() {
        return maturity_date;
    }

    public void setMaturity_date(String maturity_date) {
        this.maturity_date = maturity_date;
    }

    public float getTotal_payout_amount() {
        return total_payout_amount;
    }

    public void setTotal_payout_amount(String total_payout_amount) {
        this.total_payout_amount = Float.parseFloat( total_payout_amount );
    }

    public float getInterest_amount() {
        return interest_amount;
    }

    public void setInterest_amount(String interest_amount) {
        this.interest_amount = Float.parseFloat( interest_amount );
    }

    public float getSource_tax() {
        return source_tax;
    }

    public void setSource_tax(String source_tax) {
        this.source_tax = Float.parseFloat( source_tax );
    }

    public float getPayout_exchange_rate() {
        return payout_exchange_rate;
    }

    public void setPayout_exchange_rate(String payout_exchange_rate) {
        this.payout_exchange_rate = Float.parseFloat( payout_exchange_rate );
    }

    public String getPayout_exchange_date() {
        return payout_exchange_date;
    }

    public void setPayout_exchange_date(String payout_exchange_date) {
        this.payout_exchange_date = payout_exchange_date;
    }

    public float getPayout_date() {
        return payout_date;
    }

    public void setPayout_date(float payout_date) {
        this.payout_date = payout_date;
    }

    public float getDeposit_status() {
        return deposit_status;
    }

    public void setDeposit_status(float deposit_status) {
        this.deposit_status = deposit_status;
    }

    public Documents getDocuments() {
        return documents;
    }

    public void setDocuments(Documents documents) {
        this.documents = documents;
    }
}
