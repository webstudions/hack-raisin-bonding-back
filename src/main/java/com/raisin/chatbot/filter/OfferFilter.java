package com.raisin.chatbot.filter;

import com.raisin.chatbot.models.api.Offer;

import java.util.ArrayList;
import java.util.List;

public class OfferFilter {
    private ArrayList<Offer> offers;

    public OfferFilter(ArrayList<Offer> offers) {
        this.offers = offers;
    }

    public Offer getBestOfferFor( int  minTermMonthLength , int maxTermMonthLength , int investmentAmount) {
        Offer best = null;
        for( Offer o : offers ) {
            if( o.getTermMonthsCount() >= minTermMonthLength &&
                    o.getTermMonthsCount() <= maxTermMonthLength &&
                    investmentAmount >= o.getMinimumInvestmentAmount() &&
                    o.getMaximumInvestmentAmount() >= investmentAmount )
                if (best == null || best.getNominalInterestRate() < o.getNominalInterestRate())
                    best = o;
        }
        return best;
    }

    public ArrayList<Offer> getOffers() {
        return offers;
    }

    public void setOffers(ArrayList<Offer> offers) {
        this.offers = offers;
    }
}
