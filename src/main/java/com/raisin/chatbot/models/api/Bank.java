package com.raisin.chatbot.models.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "maximum_deposit_amount_per_customer",
        "display_name",
        "display_description",
        "bank_logo_uri",
        "country_code",
        "documents"
})
public class Bank {

    @JsonProperty("id")
    private String id;
    @JsonProperty("maximum_deposit_amount_per_customer")
    private Integer maximumDepositAmountPerCustomer;
    @JsonProperty("display_name")
    private String displayName;
    @JsonProperty("display_description")
    private String displayDescription;
    @JsonProperty("bank_logo_uri")
    private String bankLogoUri;
    @JsonProperty("country_code")
    private String countryCode;
    @JsonProperty("documents")
    private List<Document> documents = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("maximum_deposit_amount_per_customer")
    public Integer getMaximumDepositAmountPerCustomer() {
        return maximumDepositAmountPerCustomer;
    }

    @JsonProperty("maximum_deposit_amount_per_customer")
    public void setMaximumDepositAmountPerCustomer(Integer maximumDepositAmountPerCustomer) {
        this.maximumDepositAmountPerCustomer = maximumDepositAmountPerCustomer;
    }

    @JsonProperty("display_name")
    public String getDisplayName() {
        return displayName;
    }

    @JsonProperty("display_name")
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @JsonProperty("display_description")
    public String getDisplayDescription() {
        return displayDescription;
    }

    @JsonProperty("display_description")
    public void setDisplayDescription(String displayDescription) {
        this.displayDescription = displayDescription;
    }

    @JsonProperty("bank_logo_uri")
    public String getBankLogoUri() {
        return bankLogoUri;
    }

    @JsonProperty("bank_logo_uri")
    public void setBankLogoUri(String bankLogoUri) {
        this.bankLogoUri = bankLogoUri;
    }

    @JsonProperty("country_code")
    public String getCountryCode() {
        return countryCode;
    }

    @JsonProperty("country_code")
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @JsonProperty("documents")
    public List<Document> getDocuments() {
        return documents;
    }

    @JsonProperty("documents")
    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
