package com.raisin.chatbot.models.request;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "source",
        "intentId",
        "action",
        "intentName",
        "webhookUsed",
        "webhookResponseTime",
        "webhookTriggeringEvent"
})
public class ExecutionSequence {

    @JsonProperty("source")
    private String source;
    @JsonProperty("intentId")
    private String intentId;
    @JsonProperty("action")
    private String action;
    @JsonProperty("intentName")
    private String intentName;
    @JsonProperty("webhookUsed")
    private Boolean webhookUsed;
    @JsonProperty("webhookResponseTime")
    private String webhookResponseTime;
    @JsonProperty("webhookTriggeringEvent")
    private String webhookTriggeringEvent;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("source")
    public String getSource() {
        return source;
    }

    @JsonProperty("source")
    public void setSource(String source) {
        this.source = source;
    }

    @JsonProperty("intentId")
    public String getIntentId() {
        return intentId;
    }

    @JsonProperty("intentId")
    public void setIntentId(String intentId) {
        this.intentId = intentId;
    }

    @JsonProperty("action")
    public String getAction() {
        return action;
    }

    @JsonProperty("action")
    public void setAction(String action) {
        this.action = action;
    }

    @JsonProperty("intentName")
    public String getIntentName() {
        return intentName;
    }

    @JsonProperty("intentName")
    public void setIntentName(String intentName) {
        this.intentName = intentName;
    }

    @JsonProperty("webhookUsed")
    public Boolean getWebhookUsed() {
        return webhookUsed;
    }

    @JsonProperty("webhookUsed")
    public void setWebhookUsed(Boolean webhookUsed) {
        this.webhookUsed = webhookUsed;
    }

    @JsonProperty("webhookResponseTime")
    public String getWebhookResponseTime() {
        return webhookResponseTime;
    }

    @JsonProperty("webhookResponseTime")
    public void setWebhookResponseTime(String webhookResponseTime) {
        this.webhookResponseTime = webhookResponseTime;
    }

    @JsonProperty("webhookTriggeringEvent")
    public String getWebhookTriggeringEvent() {
        return webhookTriggeringEvent;
    }

    @JsonProperty("webhookTriggeringEvent")
    public void setWebhookTriggeringEvent(String webhookTriggeringEvent) {
        this.webhookTriggeringEvent = webhookTriggeringEvent;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
