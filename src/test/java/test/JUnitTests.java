package test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.raisin.chatbot.models.api.Offer;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class JUnitTests {

    @Test
    public void whenDeserializingUsingJsonCreator_thenCorrect()
            throws IOException {

        String json = "{\n" +
                "            \"id\": \"FSE001\",\n" +
                "                \"bank_id\": \"FBNIGB2L\",\n" +
                "                \"offer_type\": \"FIXED_DEPOSIT\",\n" +
                "                \"interest_payout_scheme\": \"MATURITY\",\n" +
                "                \"minimum_investment_amount\": 2000,\n" +
                "                \"maximum_investment_amount\": 100000,\n" +
                "                \"deposit_insurance_description\": \"string\",\n" +
                "                \"nominal_interest_rate\": 1.5,\n" +
                "                \"effective_interest_rate\": 1,\n" +
                "                \"currency_code\": \"EUR\",\n" +
                "                \"term_months_count\": 12\n" +
                "        }";

        Offer bean = new ObjectMapper()
                .readerFor(Offer.class)
                .readValue(json);
        assertEquals("FSE001", bean.getId());
    }

}
