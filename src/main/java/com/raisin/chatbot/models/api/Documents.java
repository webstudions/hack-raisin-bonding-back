package com.raisin.chatbot.models.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Documents {
    @JsonProperty
    private String display_text;

    @JsonProperty
    private String type;

    @JsonProperty
    private String group;

    @JsonProperty
    private String id;

    @JsonProperty
    private boolean mandatory;

    @JsonProperty
    private String mime_type;

    @JsonProperty
    private String status_code;

    @JsonProperty
    private String methods;

    @JsonProperty
    private String request_uri;

    @JsonProperty
    private String creation_date_time;

    @JsonProperty
    private String modification_date_time;

    public String getDisplay_text() {
        return display_text;
    }

    public void setDisplay_text(String display_text) {
        this.display_text = display_text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }

    public String getMime_type() {
        return mime_type;
    }

    public void setMime_type(String mime_type) {
        this.mime_type = mime_type;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMethods() {
        return methods;
    }

    public void setMethods(String methods) {
        this.methods = methods;
    }

    public String getRequest_uri() {
        return request_uri;
    }

    public void setRequest_uri(String request_uri) {
        this.request_uri = request_uri;
    }

    public String getCreation_date_time() {
        return creation_date_time;
    }

    public void setCreation_date_time(String creation_date_time) {
        this.creation_date_time = creation_date_time;
    }

    public String getModification_date_time() {
        return modification_date_time;
    }

    public void setModification_date_time(String modification_date_time) {
        this.modification_date_time = modification_date_time;
    }


}
