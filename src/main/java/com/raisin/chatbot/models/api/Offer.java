package com.raisin.chatbot.models.api;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "bank_id",
        "offer_type",
        "interest_payout_scheme",
        "minimum_investment_amount",
        "maximum_investment_amount",
        "deposit_insurance_description",
        "nominal_interest_rate",
        "effective_interest_rate",
        "currency_code",
        "term_months_count",
        "documents"
})
public class Offer {

    @JsonProperty("id")
    private String id;
    @JsonProperty("bank_id")
    private String bankId;
    @JsonProperty("offer_type")
    private String offerType;
    @JsonProperty("interest_payout_scheme")
    private String interestPayoutScheme;
    @JsonProperty("minimum_investment_amount")
    private Integer minimumInvestmentAmount;
    @JsonProperty("maximum_investment_amount")
    private Integer maximumInvestmentAmount;
    @JsonProperty("deposit_insurance_description")
    private String depositInsuranceDescription;
    @JsonProperty("nominal_interest_rate")
    private Double nominalInterestRate;
    @JsonProperty("effective_interest_rate")
    private Integer effectiveInterestRate;
    @JsonProperty("currency_code")
    private String currencyCode;
    @JsonProperty("term_months_count")
    private Integer termMonthsCount;
    @JsonProperty("documents")
    private List<Document> documents = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("bank_id")
    public String getBankId() {
        return bankId;
    }

    @JsonProperty("bank_id")
    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    @JsonProperty("offer_type")
    public String getOfferType() {
        return offerType;
    }

    @JsonProperty("offer_type")
    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }

    @JsonProperty("interest_payout_scheme")
    public String getInterestPayoutScheme() {
        return interestPayoutScheme;
    }

    @JsonProperty("interest_payout_scheme")
    public void setInterestPayoutScheme(String interestPayoutScheme) {
        this.interestPayoutScheme = interestPayoutScheme;
    }

    @JsonProperty("minimum_investment_amount")
    public Integer getMinimumInvestmentAmount() {
        return minimumInvestmentAmount;
    }

    @JsonProperty("minimum_investment_amount")
    public void setMinimumInvestmentAmount(Integer minimumInvestmentAmount) {
        this.minimumInvestmentAmount = minimumInvestmentAmount;
    }

    @JsonProperty("maximum_investment_amount")
    public Integer getMaximumInvestmentAmount() {
        return maximumInvestmentAmount;
    }

    @JsonProperty("maximum_investment_amount")
    public void setMaximumInvestmentAmount(Integer maximumInvestmentAmount) {
        this.maximumInvestmentAmount = maximumInvestmentAmount;
    }

    @JsonProperty("deposit_insurance_description")
    public String getDepositInsuranceDescription() {
        return depositInsuranceDescription;
    }

    @JsonProperty("deposit_insurance_description")
    public void setDepositInsuranceDescription(String depositInsuranceDescription) {
        this.depositInsuranceDescription = depositInsuranceDescription;
    }

    @JsonProperty("nominal_interest_rate")
    public Double getNominalInterestRate() {
        return nominalInterestRate;
    }

    @JsonProperty("nominal_interest_rate")
    public void setNominalInterestRate(Double nominalInterestRate) {
        this.nominalInterestRate = nominalInterestRate;
    }

    @JsonProperty("effective_interest_rate")
    public Integer getEffectiveInterestRate() {
        return effectiveInterestRate;
    }

    @JsonProperty("effective_interest_rate")
    public void setEffectiveInterestRate(Integer effectiveInterestRate) {
        this.effectiveInterestRate = effectiveInterestRate;
    }

    @JsonProperty("currency_code")
    public String getCurrencyCode() {
        return currencyCode;
    }

    @JsonProperty("currency_code")
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @JsonProperty("term_months_count")
    public Integer getTermMonthsCount() {
        return termMonthsCount;
    }

    @JsonProperty("term_months_count")
    public void setTermMonthsCount(Integer termMonthsCount) {
        this.termMonthsCount = termMonthsCount;
    }

    @JsonProperty("documents")
    public List<Document> getDocuments() {
        return documents;
    }

    @JsonProperty("documents")
    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
