package com.raisin.chatbot.models.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BankID {

    @JsonProperty
    public String code;

    @JsonProperty
    public String bankCode;

    @JsonProperty
    public String countryCode;

    @JsonProperty
    public String locationCode;

    @JsonProperty
    public String branchCode;
}
