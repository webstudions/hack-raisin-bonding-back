package com.raisin.chatbot.models.request;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "max-term",
        "min-term",
        "min-amount"
})
public class Parameters {

    @JsonProperty("max-term")
    private Integer maxTerm;
    @JsonProperty("min-term")
    private Integer minTerm;
    @JsonProperty("min-amount")
    private Integer minAmount;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("max-term")
    public Integer getMaxTerm() {
        return maxTerm;
    }

    @JsonProperty("max-term")
    public void setMaxTerm(Integer maxTerm) {
        this.maxTerm = maxTerm;
    }

    @JsonProperty("min-term")
    public Integer getMinTerm() {
        return minTerm;
    }

    @JsonProperty("min-term")
    public void setMinTerm(Integer minTerm) {
        this.minTerm = minTerm;
    }

    @JsonProperty("min-amount")
    public Integer getMinAmount() {
        return minAmount;
    }

    @JsonProperty("min-amount")
    public void setMinAmount(Integer minAmount) {
        this.minAmount = minAmount;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}